# C&CZ Data repository

In dit repository staan configuratie files die op machines terecht moeten komen. Deze distributie
gaat via Git en een tool die dat kan beheren. Deze tool heet
[gitopper](https://github.com/miekg/gitopper). Het bestaat uit een server en client, beide zijn ook
voor C&CZ beschikbaar als debian package via [het pkg
repository](https://gitlab.science.ru.nl/cncz/sys/pkg), gitopper en gitopperctl.

## Repository Layout

Elke process dat configuratie krijgt heeft een subdirectory in dit repository. Gitopper zelf wordt
ook beheert via de configuratie die in de `gitopper` directory staat. Het Debian package dat we zelf
bouwen weet dat _dit_ repository bestaat en gebruikt het ook in de default set up.

Voor elke directory bestaat een `README_<directory>.md` die documentatie bevat.

De gitopper/config.toml verteld welke machine, welke files moet krijgen.

Alles wat in deze subdirectories wordt geplaats komt als zodanig op het doel system terecht. Als er
bestanden zijn die eerst bewerkt moeten worden (zie uitleg over Grafana), dat gebeurd dit in *dit*
repository via een Gitlab pipeline en worden de *gegenereerde* bestanden terug gecommit *in dit
repository*.

## Generatie van Bestanden

Om inzichtelijk te houden wat er nu precies op een doel systeem wordt neer gezet willen we de
conversie zo veel mogelijk hier in Gitlab houden. Door inspectie van de `.gitlab-ci.yml` file kunnen
we precies zien wat er gebeurd.

Dit is in eerste instantie uitgevoerd voor Grafana dashboards, welke in yaml formaat in
grafana/yaml staan en dan via de gitlab pipeline worden geconverteerd naar json bestanden
in the directory grafana/dashboards. Dit wordt dan gecommit in hetzelfde repository.
Dit commiten gaat via een [project level access
token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html), dit token
(git-push-access, maintainer role, scope: write_repository) is gegenereerd en toevoegd in dit project als
een variable genaamd ACCESS_TOKEN. Zie de .gitlab-ci.yml over het gebruik. Om cycles te voorkomen
commiten we me '[skip-ci].

Zie ook [dit stack overflow
antwoord](https://stackoverflow.com/questions/51716044/how-do-i-push-to-a-repo-from-within-a-gitlab-ci-pipeline).
