Hier staan de dashboards die argos' grafana leest en weergeeft.

De sources van de dashboards staan in yaml en worden via een gitlab pipeline naar json dashboards
worden geconverteerd die in de grafana/dashboards staan.

De dashboards worden door
[graaf](https://gitlab.science.ru.nl/cncz/go/-/blob/main/cmd/graaf/README.md) gegenereed. In die
README staat ook uitgelegd hoe je een yaml dashboard moet maken en referenties naar Grafana source
code voor bv welke Y-as units worden er nu allemaal ondersteund.

## Rate en duration

We scrapen in prometheus elke 30s, als je een rate doet, gebruikt dan '2m' zodat er zeker data
punten zijn om dat goed uit te voeren.

## Style

Voor de huidige dashboards is de volgende style gebruikt:

* Begin met een hoofletter, daarna niet meer, tenzij nodig, i.e. Ceph of I/O.
* De unit geeft aan wat dashboard weer geeft dus hoeft niet ook in de titel.
* `instance` is naam van een machine die we monitoren.
